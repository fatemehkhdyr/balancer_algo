package main

import (
	"fmt"
	"math/big"
	"github.com/ALTree/bigfloat"
)


func get_amount_out(amountin *big.Float, weightin *big.Float, weightout *big.Float, balance_in *big.Float, balance_out *big.Float) (result *big.Float){
	amount_out := new(big.Float)
	amount_out_decimals := new(big.Float)
	weight := new(big.Float)
	balance := new(big.Float)
	balancediv := new(big.Float)
	balancepow := new(big.Float)
	balance.Add(balance_in, amountin)
	weight.Quo(weightin, weightout)
	balancediv.Quo(balance_in, balance)
	balancepow = bigfloat.Pow(balancediv, weight)
	balancesubone := new(big.Float)
	balancesubone.Sub(big.NewFloat(1),balancepow)
	amount_out_decimals.Mul(balance_out, balancesubone)
	decimal_pow := new(big.Float)
	decimal_pow = bigfloat.Pow(big.NewFloat(10), big.NewFloat(18))
	amount_out.Quo(amount_out_decimals, decimal_pow)
	result = amount_out
	return result


}

func main() {

	amountin, weightin, weightout, balance_in, balance_out := new(big.Float), new(big.Float), new(big.Float), new(big.Float), new(big.Float)

	amountin.SetString("196078431372549019607")
	weightin.SetString("250000000000000000")
	weightout.SetString("500000000000000000")
	balance_in.SetString("76590367347268096845066")
	balance_out.SetString("125701269226708625952")
	fmt.Println(get_amount_out(amountin, weightin, weightout, balance_in, balance_out))

}